package client;

/** Клиент
 * В конструктор передается host и номер порта
 * Метод remoteCall блокируется до получения ответа от сервера
 * */

import org.apache.log4j.Logger;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;

public class Client {
    private static final Logger log = Logger.getLogger(Client.class);
    private Integer commandId = 1;
    private final String host;
    private final int port;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Object remoteCall(String serviceName, String methodName, Object[] params) {
        Object response;
        try (Socket socket = new Socket(InetAddress.getByName(host), port)) {
            try (ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream input = new ObjectInputStream(socket.getInputStream())){

                sendRequest(serviceName, methodName, params, output);
                response = readResponse(input);
                commandId++;
            } catch (IOException e) {
                log.error("Error by client work " + e);
                return "Error by client work";
            }
        } catch (IOException e) {
            log.error("Error by client creat " + e);
            return "Error by client creat";
        }
        return response;
    }

    //Отправка запроса на сервер
    private void sendRequest(String serviceName, String methodName, Object[] params, ObjectOutputStream output) throws IOException {
        output.writeUTF(String.valueOf(commandId));
        output.writeUTF(serviceName);
        output.writeUTF(methodName);
        output.writeObject(params);
        output.flush();
        log.info("Request: number=" + commandId + ", service=" + serviceName + ", method=" + methodName + ", params=" + Arrays.toString(params));
    }

    //Получение ответа
    private Object readResponse(ObjectInputStream input) throws IOException {
        String number = input.readUTF();
        Object message = input.readUTF();
        log.info("Response: number = " + number + " message=" + message);
        return message;
    }
}
