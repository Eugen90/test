package client;

public class MyClient {
    public static void main(String[] args) {
        Client client = new Client("localhost", 6535);
        Object message;
        message = client.remoteCall("dateService", "getAGivenDate",
                new Object[] {new Integer(2020), new Integer(10), new Integer(11)});

        System.out.println(message);
        message = client.remoteCall("dateService", "getCurrentDate",
                new Object[] {});
        System.out.println(message);

        message = client.remoteCall("genderService", "getGender",
                new Object[] {new Character('f')});
        System.out.println(message);

        message = client.remoteCall("genderService", "genderState",
                new Object[]{new Boolean(true)});
        System.out.println(message);
    }
}