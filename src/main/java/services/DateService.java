package services;

import java.util.Date;

public class DateService {
    public Date getCurrentDate() {
        return new Date();
    }

    public String getAGivenDate(Integer year, Integer month, Integer day) {
        return day + "." + month + "." + year;
    }
}
