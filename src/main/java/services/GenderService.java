package services;

public class GenderService {
    public String getGender(Character character) {
        if (character == 'm') return "Мужской";
        if (character == 'w') return "Женский";
        return "не определен";
    }

    public String genderState(Boolean bool) {
        if (bool) {
            return "I am a man";
        }
        return "I am a woman";
    }
}
