package server;

/** Работа сервера
 * С помощью reflection получает доступ к запрошенному методу и выполняет его
 * Для вызова методов с разным количеством параметров придуман костыль в виде метода runMethod.
 * В зависимости от количества переданных параметров он вызывает метод с этими параметрами.
 * */

import org.apache.log4j.Logger;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ConcurrentMap;

public class ServerCaller implements Runnable {
    private static final Logger log = Logger.getLogger(ServerCaller.class);
    private final Socket serverSocket;
    private final ConcurrentMap<String, Object> services;

    public ServerCaller(Socket serverSocket, ConcurrentMap<String, Object> services) {
        this.serverSocket = serverSocket;
        this.services = services;
    }

    @Override
    public void run() {
        try (ObjectInputStream input = new ObjectInputStream(serverSocket.getInputStream());
             ObjectOutputStream output = new ObjectOutputStream(serverSocket.getOutputStream())) {

            String number = input.readUTF();
            String serviceName = input.readUTF();
            String methodName = input.readUTF();
            Object[] params = (Object[]) input.readObject();
            log.info("Request: number=" + number + ", service=" + serviceName + ", method=" + methodName + ", params=" + Arrays.toString(params));

            Object obj = services.get(serviceName);
            Object result = execute(obj, methodName, params);

            sendRequest(result, number, output);

            output.flush();
            serverSocket.close();
        } catch (IOException | ClassNotFoundException e) {
            log.error("Error by server " + e);
        }
    }

    //Ответ клиенту
    private void sendRequest(Object result, String number, ObjectOutputStream output) throws IOException {
        output.writeUTF(number);
        output.writeUTF(result.toString());
        log.info("Response: message=" + result);
    }

    //Выполнение запрошенного метода
    private static Object execute(Object object, String methodName, Object[] params) {
        Method[] declaredMethods = object.getClass().getDeclaredMethods();
        for (Method method: declaredMethods) {
            if (method.getName().equals(methodName)) {
                if (method.getParameterCount() != params.length) {
                    log.error("The number of parameters in the method and the number of passed parameters does not match");
                    return "The number of parameters in the method and the number of passed parameters does not match";
                }
                for (int i = 0; i < method.getParameterCount(); i++) {
                    if (method.getParameterTypes()[0] != params[0].getClass()) {
                        log.error("Parameter type " + (i + 1) + " does not match");
                        return "Parameter type " + (i + 1) + " does not match";
                    }
                }
                return runMethod(method, object, params);
            }
        }
        log.error("Method named " + methodName + " not found");
        return "Method named " + methodName + " not found";
    }

    //Вызов метода с заданными параметрами (сделано через костыль, но как сделать по-другому не знаю!!)
    private static Object runMethod(Method method, Object obj, Object[] params) {
        Object object;
        try {
            switch (method.getParameterCount()) {
                case 1:
                    object = method.invoke(obj, params[0]);
                    break;
                case 2:
                    object = method.invoke(obj, params[0], params[1]);
                    break;
                case 3:
                    object = method.invoke(obj, params[0], params[1], params[2]);
                    break;
                case 4:
                    object = method.invoke(obj, params[0], params[1], params[2], params[3]);
                    break;
                case 5:
                    object = method.invoke(obj, params[0], params[1], params[2], params[3], params[4]);
                    break;
                default:
                    object = method.invoke(obj);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("Method invoke error " + e);
            return "Method invoke error";
        }
        if (method.getReturnType().equals(Void.TYPE)) return "The return value of method " + method.getName() + " is void";
        return object;
    }
}
