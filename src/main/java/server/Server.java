package server;

/** Сервер
 *  Порт сервера необходимо передовать аргументом.
 *  Используется пул из 50 потоков
 *  Каждый запрос к серверу выполняется в отдельном потоке
 * */

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final Logger log = Logger.getLogger(Server.class);
    private static final ExecutorService executeService = Executors.newFixedThreadPool(50);

    //Работа сервера
    private static void run(String[] args) {
        if (args.length == 0) {
            log.error("Port number argument is empty");
            return;//Прерывание по причине отсутствия номера порта в аргументах вызова
        }
        if (!args[0].matches("\\d{4}")) {
            log.error("Unable to read port number \"" + args[0] + "\"");
            return; //Прерывание по причине не соответствия формата порта
        }
        final int PORT = Integer.parseInt(args[0]);
        ConcurrentMap<String, Object> services = createServices();
        try(ServerSocket server = new ServerSocket(PORT)) {
            log.info("The server started on localhost, on port " + PORT);
            while (true) {
                Socket socket = server.accept();
                executeService.execute(new ServerCaller(socket, services));
            }
        } catch (IOException e) {
            log.error("Error by server start: " + e);
        }
    }

    //Создание map-ы сервисов из файла properties. Ключ - название сервиса, значение - экземпляр сервиса
    private static ConcurrentMap<String, Object> createServices() {
        ConcurrentMap<String, Object> objects = new ConcurrentHashMap<>();
        Enumeration<?> properties = PropertyReader.getInstance().getPropertiesList();
        while (properties.hasMoreElements()) {
            String property = String.valueOf(properties.nextElement());
            String className = PropertyReader.getInstance().getPropertyByName(property);
            try {
                Object object = Class.forName("services." + className).newInstance();
                objects.put(property, object);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                log.error("Error while trying to create a service " + className + " " + e);
            }
        }
        return objects;
    }

    public static void main(String[] args) {
        run(args);
    }
}
