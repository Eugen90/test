package server;

/**
 *  Чтение properties.
 *  Для экономии ресурсов использоется патерн singleton
 * */

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import org.apache.log4j.Logger;

public class PropertyReader {
    private final Properties properties = new Properties();
    private static final Logger log = Logger.getLogger(PropertyReader.class);
    private static PropertyReader instance = new PropertyReader();

    public static PropertyReader getInstance() {
        synchronized (PropertyReader.class) {
            if (instance == null) instance = new PropertyReader();
        }
        return instance;
    }

    private PropertyReader() {
        String PROPERTIES_PATH = "server.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try(InputStream input = loader.getResourceAsStream(PROPERTIES_PATH)) {
            properties.load(input);
            log.info("Property file " + PROPERTIES_PATH + " was read successfully");
        } catch (IOException e) {
            log.error("Property file " + PROPERTIES_PATH + " not found");
        }
    }

    public String getPropertyByName(String propertyName) {
        return properties.getProperty(propertyName);
    }

    public Enumeration<?> getPropertiesList() {
        return properties.propertyNames();
    }
}
